import {  ENV  } from './types';
import validateAddresses from './utils/validateAddresses';
import { ethers } from 'ethers';
import {  getProviders } from './utils/provider.utils';
import { fetchABIs } from './utils/fetchABIs';
import { NFT_ADDRESS, TOKEN_ADDRESS } from './utils/constants';
import * as banksyOperations from "../src/lib/banksyNFT";
export class Banksy {
  env: ENV;
  NFTABI: any;
  provider: undefined | ethers.providers.JsonRpcProvider;
  NFTAddress : string | undefined;
  tokenAddress : string | undefined;
  constructor(env: ENV = 'testnet') {
    this.env = env;
  }
  async init() {
    try {
      this.provider = await getProviders(this.env);
      const { NFTABI } = await fetchABIs(this.env);
      this.NFTABI = NFTABI;
      this.NFTAddress = NFT_ADDRESS[this.env];
      this.tokenAddress = TOKEN_ADDRESS[this.env];

    } catch (error: any) {
      throw new Error(`Initializing  : ${error?.message}`);
    }
  }

  getWallet(key: any) {
    try {
      if (!this.provider) throw new Error('Providers are not defined');
      if (typeof key === 'string') return new ethers.Wallet(key, this.provider);
      else return key;
    } catch (error) {
      throw new Error('Couldnt generate Wallet');
    }
  }
 
  mintNFT(receiver : string,uri : string,amount : string,key: any): Promise<any> {
    try {
      validateAddresses(receiver);
      if(!this.NFTAddress) throw new Error("SDK Not Initialised");
      return banksyOperations.mintNFT(this.NFTABI,this.NFTAddress,receiver,uri,amount,this.getWallet(key))
    } catch (error) {
      throw error;
    }
  }

  burnNFT(tokenId : string,key: any): Promise<any> {
    try {
      if(!this.NFTAddress) throw new Error("SDK Not Initialised");
      return banksyOperations.burnNFT(this.NFTABI,this.NFTAddress,tokenId,this.getWallet(key))
    } catch (error) {
      throw error;
    }
  }
 
}
