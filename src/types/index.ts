import { ethers } from 'ethers';

export type ENV = 'mainnet' | 'testnet';
