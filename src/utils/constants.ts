import { ENV } from "../types";

export const BC_STACK_URL = {
  testnet: 'https://blockchain-gateway-sandbox.volary.io/apiprocess',
  mainnet: 'https://blockchain-gateway.prod.volary.io/apiprocess',
};

export const NFT_ADDRESS : {[key in ENV] : string}= {
  "mainnet" : "",
  "testnet" : "0xB9895ed303f46c72254B50bF33B9336c196570df"
}

export const TOKEN_ADDRESS : {[key in ENV] : string} = {
  "mainnet" : "",
  "testnet" : "0x22B0062bDc2f5803f73AbFcCe37dc9636b131a84"
}