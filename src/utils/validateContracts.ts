import { ethers } from 'ethers';

export default async (address: string, provider: ethers.providers.JsonRpcProvider) => {
  if (!ethers.utils.isAddress(address)) throw new Error('Invalid Contract Address');
  const contractCode = await provider.getCode(address);
  if (contractCode === '0x') throw new Error('Given Address is not a contract');
};
