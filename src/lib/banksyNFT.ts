import { ethers } from "ethers";
import validateContracts from "../utils/validateContracts";
import { parseEthersError } from "../utils/EthersError";
import { toEther, toWei } from "../utils/conversions";


export const mintNFT = async (
    ABI: ethers.ContractInterface,
    NFTAddress: string,
    receiver: string,
    uri : string,
    amount: string,
    wallet: any,
    gasOptions?: any,
  ) => {
    try {
      await validateContracts(NFTAddress, wallet.provider);
      const contract = new ethers.Contract(NFTAddress, ABI, wallet);
      try {
        const receipt = await contract.mint(receiver,uri, toWei(amount),"1","0x")
        await receipt.wait();
        return {
          code: 1,
          hash: receipt?.hash,
        };
      } catch (error) {
        throw new Error(parseEthersError(error));
      }
    } catch (error: any) {
      throw error;
    }
  };

export const burnNFT = async (
    ABI: ethers.ContractInterface,
    NFTAddress : string,
    tokenId : string,
    wallet: any,
    gasOptions?: any,
  ) => {
    try {
      await validateContracts(NFTAddress, wallet.provider);
      const contract = new ethers.Contract(NFTAddress, ABI, wallet);
      try {
        const receipt = await contract.burnNFT(tokenId,"1")
        await receipt.wait();
        return {
          code: 1,
          hash: receipt?.hash,
        };
      } catch (error) {
        throw new Error(parseEthersError(error));
      }
    } catch (error: any) {
      throw error;
    }
  };


